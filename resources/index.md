---
# This file was generated from `meta.yml`, please do not edit manually.
# Follow the instructions on https://github.com/coq-community/templates to regenerate.
title: Small Step Reflection Maps
lang: en
header-includes:
  - |
    <style type="text/css"> body {font-family: Arial, Helvetica; margin-left: 5em; font-size: large;} </style>
    <style type="text/css"> h1 {margin-left: 0em; padding: 0px; text-align: center} </style>
    <style type="text/css"> h2 {margin-left: 0em; padding: 0px; color: #580909} </style>
    <style type="text/css"> h3 {margin-left: 1em; padding: 0px; color: #C05001;} </style>
    <style type="text/css"> body { width: 1100px; margin-left: 30px; }</style>
---

## About

Welcome to the Small Step Reflection Maps project website!

This library provides implementation for finite maps and  sets. The
implemetaton follows similar metheology to that of `coq-stdpp`. The
implementation is based on:
  Efficient Extensional Binary Tries by Andrew W Appel and Xavier Leroy

This is an open source project, licensed under the Apache License 2.0.

## Get the code

The current stable release of Small Step Reflection Maps can be [downloaded from Gitlab](https://gitlab.com/thewalker77/ssrmap/-/packages).

## Documentation


Related publications, if any, are listed below.


## Help and contact

- Report issues on [Gitlab](https://gitlab.com/thewalker77/ssrmap/issues)

## Authors and contributors

- thewalker77

