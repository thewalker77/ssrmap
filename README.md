<!---
This file was generated from `meta.yml`, please do not edit manually.
Follow the instructions on https://github.com/coq-community/templates to regenerate.
--->
# Small Step Reflection Maps

[![coqdoc][coqdoc-shield]][coqdoc-link]



[coqdoc-shield]: https://img.shields.io/badge/docs-coqdoc-blue.svg
[coqdoc-link]: https://thewalker77.gitlab.io/ssrmap


This library provides implementation for finite maps and  sets. The
implemetaton follows similar metheology to that of `coq-stdpp`. The
implementation is based on:
  Efficient Extensional Binary Tries by Andrew W Appel and Xavier Leroy

## Meta

- Author(s):
  - thewalker77
- License: [Apache License 2.0](LICENSE)
- Compatible Coq versions: 8.15.x and 8.16.x
- Additional dependencies:
  - [MathComp](https://math-comp.github.io) 1.15.0 or later
  - [Mczify](https://github.com/math-comp/mczify) 1.2 or later
  - [Dune](https://dune.build) 2.5 or later
- Coq namespace: `ssrmap`
- Related publication(s): none

## Building and installation instructions

### Installing the dependencies

To build and install manually, First make sure you have
[opam](https://opam.ocaml.org/). Clone the repository and install the needed
dependencies as follows:

```shell
git clone https://gitlab.com/thewalker77/ssrmap.git
cd ssrmap
opam install ./coq-ssrmap.opam --deps-only
```

### Building using `_CoqProject`

```shell
make -j`nproc`
```

### Building and installation using dune

``` shell
dune build
dune install
```



