(*****************************************************************************)
(* Copyright 2022 TheWalker77                                                *)
(*                                                                           *)
(* Licensed under the Apache License, Version 2.0 (the "License");           *)
(* you may not use this file except in compliance with the License.          *)
(* You may obtain a copy of the License at                                   *)
(*                                                                           *)
(*     http://www.apache.org/licenses/LICENSE-2.0                            *)
(*                                                                           *)
(* Unless required by applicable law or agreed to in writing, software       *)
(* distributed under the License is distributed on an "AS IS" BASIS,         *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  *)
(* See the License for the specific language governing permissions and       *)
(* limitations under the License.                                            *)
(*****************************************************************************)

From mathcomp Require Import all_ssreflect zify.
From ssrmap Require Import seq.
Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.

(*****************************************************************************)
(* This file provides API for dealing with arbitrary extentional finite      *)
(* To implement a new data structure supporting this API, the implementor    *)
(* must be able to provide implementation for the operations described by    *)
(* [FinSet.operations] as well as the lemmas described by [FinSet.Lemmas].   *)
(*****************************************************************************)

Module FinSet.
Record operations (T: eqType) (S: Type -> Type) := Operations {
    Empty: S T;
    In: T -> S T -> Prop;
    Inb: T -> S T -> bool;
    Singleton: T -> S T;
    Union : S T -> S T -> S T;
    Intersection: S T -> S T -> S T;
    Difference: S T -> S T -> S T;
    ToList: S T -> seq T;
}.

Record lemmas (T: eqType) (S: Type -> Type) (ops: operations T S) := Lemmas {
    Extensionality: forall (s1 s2: S T),
                        s1 = s2 <-> forall a, (In ops a s1 <-> In ops a s2);
    DecidableIn: forall a s, reflect (In ops a s) (Inb ops a s);
    NotInEmpty: forall (a: T), ~ In ops a (Empty ops);
    InSingleton: forall (a1 a2: T), In ops a1 (Singleton ops a2) <-> a1 = a2;
    InUnion: forall (s1 s2 : S T) (a: T),
                In ops a (Union ops s1 s2) <->
                In ops a s1 \/ In ops a s2;
    InIntersection: forall (s1 s2 : S T) (a: T),
                        In ops a (Intersection ops s1 s2) <->
                        In ops a s1 /\ In ops a s2;
    InDifference: forall (s1 s2 : S T) (a: T),
                        In ops a (Difference ops s1 s2) <->
                        In ops a s1 /\ ~ In ops a s2;
    InToList: forall (s: S T) (a: T), In ops a s <-> PIn a (ToList ops s);
    UniqueToList: forall (s: S T), PUniq (ToList ops s)
}.

Structure type (T: eqType):=  Pack{
    ST: Type -> Type;
    ops: operations T ST;
    lms: lemmas ops;
}.
End FinSet.

Import FinSet.
Definition finSet := FinSet.type.
Notation SetType T e := (FinSet.ST e T).

(* base operations *)
Definition set_empty T (e: finSet T) : SetType T e := Empty (ops e).
Arguments set_empty {T e}: simpl never.
Notation "∅" := set_empty (format "∅").

Definition set_in T (e: finSet T) (a: T) (s: SetType T e) : Prop :=
    In (ops e) a s.
Arguments set_in {T e} a s: simpl never.
Infix "∈" := set_in (at level 70).
Notation "x ∉ X" := (~ x ∈ X) (at level 80).

Definition set_inb T (e: finSet T) (a: T) (s: SetType T e) : bool :=
    Inb (ops e) a s.
Arguments set_in {T e} a s: simpl never.

Definition set_singleton T (e: finSet T) (a: T) : SetType T e :=
    Singleton (ops e) a.
Arguments set_singleton {T e} a: simpl never.
Notation "{[ x ]}" := (set_singleton x) (at level 1).

Definition set_union T (e: finSet T) (s1 s2: SetType T e) : SetType T e :=
    Union (ops e) s1 s2.
Arguments set_union {T e} s1 s2: simpl never.
Infix "∪" := set_union (at level 50, left associativity).

Definition set_intersection T (e: finSet T) (s1 s2: SetType T e): SetType T e :=
    Intersection (ops e) s1 s2.
Arguments set_intersection {T e} s1 s2: simpl never.
Infix "∩" := set_intersection (at level 40, left associativity).

Definition set_difference T (e: finSet T) (s1 s2: SetType T e) : SetType T e :=
    Difference (ops e) s1 s2.
Arguments set_difference {T e} s1 s2: simpl never.
Infix "\" := set_difference (at level 40).

Definition set_to_list T (e: finSet T) (s: SetType T e) : seq T :=
    ToList (ops e) s.
Arguments set_to_list {T e} s: simpl never.

(* derived operations *)
Definition set_subseteq {T} {e: finSet T} (s1 s2: SetType T e):=
    forall a, a ∈ s1 -> a ∈ s2.
Arguments set_subseteq {T e} s1 s2: simpl never.
Infix "⊆" := set_subseteq (at level 70).
Notation "X ⊈ Y" := (~ X ⊆ Y) (at level 70).

Definition set_subset {T} {e: finSet T} (s1 s2: SetType T e):=
        s1 ⊆ s2 /\ ~ s2 ⊆ s1.
Arguments set_subset {T e} s1 s2: simpl never.
Infix "⊂" := set_subset (at level 70).
Notation "X ⊄ Y" := (~X ⊂ Y) (at level 70).

Definition set_disjoint {T} {e: finSet T} (s1 s2: SetType T e) :=
    forall a, a ∈ s1 -> a ∈ s2 -> False.
Arguments set_disjoint {T e} s1 s2: simpl never.
Infix "##" := set_disjoint (at level 70).

Definition set_forall {T} {e: finSet T} (P: T -> Prop) (s: SetType T e) :=
    forall a, a ∈ s -> P a.

Definition set_exists {T} {e: finSet T} (P: T -> Prop) (s: SetType T e) :=
    exists a, a ∈ s /\ P a.

Definition set_size {T} {e: finSet T} (s: SetType T e): nat :=
    length (set_to_list s).

Definition set_fold {T T'} {e: finSet T} (f: T -> T' -> T')
                    t (s: SetType T e) :=
    foldr f t (set_to_list s).

Fixpoint list_to_set {T} {e: finSet T} (l: seq T): SetType T e :=
match l with
| [::] => ∅
| a::l' => {[a]} ∪ list_to_set l'
end.

Definition set_map T T' {e: finSet T} {e': finSet T'} (s: SetType T e)
                   (f: T -> T') : SetType T' e' :=
    list_to_set (map f (set_to_list s)).

(* base lemmas *)
Lemma set_extensionality T (e: finSet T) (s1 s2: SetType T e):
    s1 = s2 <-> forall a, (a ∈ s1 <-> a ∈ s2).
Proof Extensionality (lms e) s1 s2.

Lemma set_inP T (e: finSet T) a (s: SetType T e) : reflect (a ∈ s) (Inb (ops e) a s).
Proof @DecidableIn _ _ _ (lms e) a s.

Lemma set_not_in_empty T (e: finSet T) (a: T) :a ∉ (∅ : SetType T e).
Proof @NotInEmpty _ _ _ (lms e) a.

Lemma set_in_singleton_iff T (e: finSet T) (a1 a2: T) :
    a1 ∈ ({[a2]} : SetType T e) <-> a1 = a2.
Proof @InSingleton _ _ _ (lms e) a1 a2.

Lemma set_in_union_iff T (e: finSet T) (s1 s2: SetType T e) a:
            a ∈ (s1 ∪ s2) <-> a ∈ s1 \/ a ∈ s2.
Proof @InUnion _ _ _ (lms e) s1 s2 a.

Lemma set_in_intersection_iff T (e: finSet T) (s1 s2: SetType T e) a:
    a ∈ (s1 ∩ s2) <-> a ∈ s1 /\ a ∈ s2.
Proof @InIntersection _ _ _ (lms e) s1 s2 a.

Lemma set_in_difference_iff T (e: finSet T) (s1 s2: SetType T e) a:
    a ∈ (s1 \ s2) <-> a ∈ s1 /\ a ∉ s2.
Proof @InDifference _ _ _ (lms e) s1 s2 a.

Lemma set_in_to_list_iff T (e: finSet T) (s: SetType T e) a:
    a ∈ s <-> PIn a (set_to_list s).
Proof @InToList _ _ _ (lms e) s a.

Lemma set_unique_to_list T (e: finSet T) (s: SetType T e):
    PUniq (set_to_list s).
Proof @UniqueToList _ _ _ (lms e) s.


(* derived lemmas *)
Section rewriting.
Context (T: eqType).
Context (e: finSet T).

(* Rewriting Lemmas about ∅`*)
Lemma set_not_in_empty_iff a: a ∈ (∅ : SetType T e) <-> False.
Proof.
split;
    last by [].
by apply: set_not_in_empty.
Qed.

Lemma set_eq_empty_iff (s: SetType T e): s = ∅ <-> forall a, a ∉ s.
Proof.
rewrite set_extensionality.
setoid_rewrite set_not_in_empty_iff.
split.
    move => Hfalse a Hin.
    by rewrite -(Hfalse a).
move => Hin a.
split => //.
apply: Hin.
Qed.

(* rewriting lemmas about subseteq/subset *)
Lemma set_subseteq_iff (s1 s2: SetType T e):
    s1 ⊆ s2 <-> forall a, a ∈ s1 -> a ∈ s2.
Proof.
by [].
Qed.

Lemma set_subset_iff (s1 s2: SetType T e):
    s1 ⊂ s2 <->
    (forall a, a ∈ s1 -> a ∈ s2) /\
    ~ (forall a, a ∈ s2 -> a ∈ s1).
Proof.
by [].
Qed.

Lemma set_union_subseteq (s1 s2 s3: SetType T e):
    s1 ∪ s2 ⊆ s3 <-> s1 ⊆ s3 /\ s2 ⊆ s3.
Proof.
rewrite !set_subseteq_iff.
setoid_rewrite set_in_union_iff.
split.
    move => Hsubset123.
    split;
        move => a Hsubset;
        apply: Hsubset123;
        by [left|right].
move => [Hsubset13 Hsubset23] a [Hin1 | Hin2].
    by apply: Hsubset13.
by apply: Hsubset23.
Qed.

Lemma set_subseteq_empty (s: SetType T e):
    s ⊆ ∅ <-> s = ∅.
Proof.
rewrite set_subseteq_iff set_extensionality.
setoid_rewrite set_not_in_empty_iff.
split;
    last first;
    move => Hin a.
    by rewrite Hin.
split => //.
by apply: Hin.
Qed.

(* rewriting lemmas about singleton *)
Lemma set_singleton_iff x y:
    ({[x]} : SetType T e) = {[y]} <-> x = y.
Proof.
rewrite set_extensionality.
setoid_rewrite set_in_singleton_iff.
split.
    by move <-.
by move ->.
Qed.

Lemma set_in_subseteq_singleton (s: SetType T e) a:
{[ a ]} ⊆ s <-> a ∈ s.
Proof.
rewrite set_subseteq_iff.
setoid_rewrite set_in_singleton_iff.
split.
    move => Hin.
    by apply: Hin.
by move => Hin a' ->.
Qed.

Lemma set_non_empty_singleton_iff a :
    {[ a ]} = (∅ : SetType T e) <-> False.
Proof.
rewrite set_extensionality.
setoid_rewrite set_in_singleton_iff.
setoid_rewrite set_not_in_empty_iff.
split => //.
move => Hneq.
by apply/(Hneq a).
Qed.

(* rewriting lemmas about union *)

Lemma set_not_in_union_iff (s1 s2: SetType T e) a:
    a ∉ s1 ∪ s2 <-> a ∉ s1 /\ a ∉ s2.
Proof.
rewrite set_in_union_iff.
split;
    last first.
    move => [Hnotin1 Hnotin2] [Hin1|Hin2].
        by apply: Hnotin1.
    by apply: Hnotin2.
move => HnotIn.
split;
    move => Hin;
    apply: HnotIn;
    by [left|right].
Qed.

Lemma set_union_id (s: SetType T e):
    s ∪ s = s.
Proof.
rewrite set_extensionality => a.
rewrite set_in_union_iff.
split.
    by move => [|].
by left.
Qed.

Lemma set_union_empty_l (s: SetType T e):
    ∅ ∪ s = s.
Proof.
rewrite set_extensionality => a.
rewrite set_in_union_iff set_not_in_empty_iff.
split.
    by move => [|].
by right.
Qed.

Lemma set_union_empty_r (s: SetType T e):
    s ∪ ∅ = s.
Proof.
rewrite set_extensionality => a.
rewrite set_in_union_iff set_not_in_empty_iff.
split.
    by move => [|].
by left.
Qed.

Lemma set_union_assoc (s1 s2 s3: SetType T e):
    s1 ∪ (s2 ∪ s3) = (s1 ∪ s2) ∪ s3.
Proof.
rewrite set_extensionality => a.
by rewrite !set_in_union_iff or_assoc.
Qed.

Lemma set_empty_union (s1 s2: SetType T e):
 s1 ∪ s2 = ∅ <-> s1 = ∅ /\ s2 = ∅.
Proof.
rewrite !set_extensionality.
setoid_rewrite set_in_union_iff.
setoid_rewrite set_not_in_empty_iff.
split;
    last first.
    move => [Hin1 Hin2] a.
    rewrite Hin1 Hin2.
    split => //.
    by move => [].
move => Hin12.
split;
    split => //=;
    rewrite -(Hin12 a);
    by [left|right].
Qed.

(* rewriting lemmas about disjoint *)
Lemma set_disjoint_iff (s1 s2: SetType T e):
    s1 ## s2 <-> forall a, a ∈ s1 -> a ∈ s2 -> False.
Proof.
by [].
Qed.

Lemma set_disjoint_singleton_l (s: SetType T e) a: {[ a ]} ## s <-> a ∉ s.
Proof.
rewrite set_disjoint_iff.
setoid_rewrite set_in_singleton_iff.
split.
    move => Hdisjoint Hin.
    by apply: (Hdisjoint a).
by move => Hnotin a' ->.
Qed.

Lemma set_disjoint_singleton_r (s: SetType T e) a: s ## {[ a ]} <-> a ∉ s.
Proof.
rewrite set_disjoint_iff.
setoid_rewrite set_in_singleton_iff.
split.
    move => Hdisjoint Hin.
    by apply: (Hdisjoint a).
move => Hnotin a' Hin Heq.
apply: Hnotin.
by rewrite -Heq.
Qed.

Lemma set_disjoint_union_l (s1 s2 s3: SetType T e):
    s1 ∪ s3 ## s2 <-> s1 ## s2 /\ s3 ## s2.
Proof.
rewrite !set_disjoint_iff.
setoid_rewrite set_in_union_iff.
split;
    last first.
    move => [Hdisjoint12 Hdisjoint32] a [|].
        by apply: Hdisjoint12.
    by apply: Hdisjoint32.
move => Hdisjoint.
split.
+   move => a Hin1 Hin2.
    apply: (Hdisjoint a) => //.
    by left.
+   move => a Hin3 Hin2.
    apply: (Hdisjoint a) => //.
    by right.
Qed.

Lemma set_disjoint_union_r (s1 s2 s3: SetType T e):
    s1 ## s2 ∪ s3 <-> s1 ## s2 /\ s1 ## s3.
Proof.
rewrite !set_disjoint_iff.
setoid_rewrite set_in_union_iff.
split;
    last first.
    move => [Hdisjoint12 Hdisjoint13] a Hin1 [|].
        by apply: Hdisjoint12.
    by apply: Hdisjoint13.
move => Hdisjoint.
split.
+   move => a Hin1 Hin2.
    apply: (Hdisjoint a) => //.
    by left.
+   move => a Hin1 Hin2.
    apply: (Hdisjoint a) => //.
    by right.
Qed.

(* rewriting lemmas for intersection *)
Lemma set_intersection_assoc (s1 s2 s3: SetType T e):
    s1 ∩ (s2 ∩ s3) = (s1 ∩ s2) ∩ s3.
Proof.
rewrite set_extensionality => a.
rewrite !set_in_intersection_iff.
split.
    by move => [Hin1 [Hin2 Hin3]].
by move => [[Hin1 Hin2] Hin3].
Qed.

Lemma set_intersection_id (s: SetType T e): s ∩ s = s.
Proof.
rewrite set_extensionality => a.
rewrite set_in_intersection_iff.
split.
    by move => [].
by [].
Qed.

Lemma set_intersection_empty_l (s: SetType T e): ∅ ∩ s = ∅.
Proof.
rewrite set_extensionality => a.
rewrite set_in_intersection_iff set_not_in_empty_iff.
split.
    by move => [].
by [].
Qed.

Lemma set_intersection_empty_r (s: SetType T e): s ∩ ∅ = ∅ .
Proof.
rewrite set_extensionality => a.
rewrite set_in_intersection_iff set_not_in_empty_iff.
split.
    by move => [].
by [].
Qed.

(* rewriting lemmas for difference *)
Lemma set_difference_twice (s1 s2: SetType T e): (s1 \ s2) \ s2 = s1 \ s2.
Proof.
rewrite set_extensionality => a.
rewrite !set_in_difference_iff.
split.
    by move => [[]].
by move => [].
Qed.

Lemma set_difference_diag (s: SetType T e): s \ s = ∅.
Proof.
rewrite set_extensionality => a.
rewrite set_in_difference_iff set_not_in_empty_iff.
split.
    by move => [].
by [].
Qed.

Lemma set_difference_empty (s: SetType T e): s \ ∅ = s.
Proof.
rewrite set_extensionality => a.
rewrite set_in_difference_iff set_not_in_empty_iff.
split.
    by move => [].
by split.
Qed.

Lemma set_empty_difference (s: SetType T e): ∅ \ s = ∅.
Proof.
rewrite set_extensionality => a.
rewrite set_in_difference_iff set_not_in_empty_iff.
split.
    by move => [].
by [].
Qed.

End rewriting.

Definition setE :=
    (set_eq_empty_iff, set_difference_diag, set_empty_difference,
     set_difference_empty, set_subseteq_empty, set_in_subseteq_singleton,
     set_intersection_empty_r, set_intersection_empty_l, set_intersection_id,
     set_union_subseteq, set_singleton_iff, set_not_in_empty_iff, set_union_id,
     set_union_empty_l, set_union_empty_r, set_in_union_iff,
     set_not_in_union_iff, set_union_assoc, set_empty_union, set_in_union_iff,
     set_intersection_assoc, set_in_intersection_iff, set_difference_twice,
     set_in_difference_iff, set_subset_iff, set_subseteq_iff,
     set_non_empty_singleton_iff, set_in_singleton_iff,
     set_disjoint_singleton_r, set_disjoint_singleton_l, set_disjoint_union_r,
     set_disjoint_union_l, set_disjoint_iff, set_extensionality).

Tactic Notation  "setoid_setE_rewrite_once" :=
try setoid_rewrite set_difference_diag;
try setoid_rewrite set_empty_difference;
try setoid_rewrite set_difference_empty;
try setoid_rewrite set_eq_empty_iff;
try setoid_rewrite set_subseteq_empty;
try setoid_rewrite set_in_subseteq_singleton;
try setoid_rewrite set_intersection_empty_r;
try setoid_rewrite set_intersection_empty_l;
try setoid_rewrite set_intersection_id;
try setoid_rewrite set_union_subseteq;
try setoid_rewrite set_singleton_iff;
try setoid_rewrite set_not_in_empty_iff;
try setoid_rewrite set_union_id;
try setoid_rewrite set_union_empty_l;
try setoid_rewrite set_union_empty_r;
try setoid_rewrite set_not_in_union_iff;
try setoid_rewrite set_empty_union;
try setoid_rewrite set_in_union_iff;
try setoid_rewrite set_in_union_iff;
try setoid_rewrite set_intersection_assoc;
try setoid_rewrite set_in_intersection_iff;
try setoid_rewrite set_difference_twice;
try setoid_rewrite set_in_difference_iff;
try setoid_rewrite set_subset_iff;
try setoid_rewrite set_subseteq_iff;
try setoid_rewrite set_non_empty_singleton_iff;
try setoid_rewrite set_in_singleton_iff;
try setoid_rewrite set_disjoint_singleton_r;
try setoid_rewrite set_disjoint_singleton_l;
try setoid_rewrite set_disjoint_union_r;
try setoid_rewrite set_disjoint_union_l;
try setoid_rewrite set_disjoint_iff;
try setoid_rewrite set_extensionality.

Tactic Notation  "setoid_setE_rewrite" := repeat setoid_setE_rewrite_once.

Section lemmas.
Context (T: eqType).
Context (e: finSet T).
Context (s1 s2 s3 s4: SetType T e).

Lemma set_eq_subseteq: s1 = s2 <-> s1 ⊆ s2 /\ s2 ⊆ s1.
Proof.
rewrite !setE.
split;
    last first.
    move => [Hsubset12 Hsubset21] a.
    split.
        by apply: Hsubset12.
    by apply: Hsubset21.
move => Hequiv.
split;
    move => a;
    by rewrite Hequiv.
Qed.

Lemma set_subseteq_antisymm : s1 ⊆ s2 -> s2 ⊆ s1 -> s1 = s2.
Proof.
by rewrite set_eq_subseteq.
Qed.

Lemma set_subseteq_trans: s1 ⊆ s2 -> s2 ⊆ s3 -> s1 ⊆ s3.
Proof.
rewrite !setE => Hsubset12 Hsubset23 a Hsubset13.
apply: Hsubset23.
by apply: Hsubset12.
Qed.

Lemma set_subseteq_union: s1 ∪ s2 = s2 <-> s1 ⊆ s2.
Proof.
rewrite !setE.
setoid_setE_rewrite.
split.
    move => Hunion a Hin2.
    rewrite <- Hunion.
    by left.
move => Hsubset a.
split.
    move => [Hin1|Hin2] //=.
    by apply: Hsubset.
by right.
Qed.

Lemma set_union_subseteq_l: s1 ⊆ s1 ∪ s2.
Proof.
rewrite !setE => a.
rewrite !setE.
by left.
Qed.

Lemma set_union_subseteq_l': s1 ⊆ s2 -> s1 ⊆ s2 ∪ s3.
Proof.
rewrite !setE => Hsubset12 a.
rewrite !setE.
left.
by apply: Hsubset12.
Qed.

Lemma set_union_subseteq_r: s2 ⊆ s1 ∪ s2.
Proof.
rewrite !setE => a.
rewrite !setE.
by right.
Qed.

Lemma set_union_subseteq_r':s2 ⊆ s3 -> s2 ⊆ s1 ∪ s3.
Proof.
rewrite !setE => Hsubset23 a Hin2.
rewrite !setE.
right.
by apply: Hsubset23.
Qed.

Lemma set_union_least:
    s1 ⊆ s3 -> s2 ⊆ s3 -> s1 ∪ s2 ⊆ s3.
Proof.
by rewrite !setE.
Qed.

Lemma set_in_weaken a: a ∈ s1 -> s1 ⊆ s2 -> a ∈ s2.
Proof.
rewrite !setE.
move => Hin Hinin.
by apply: Hinin.
Qed.

Lemma set_not_in_weaken a: a ∉ s2 -> s1 ⊆ s2 -> a ∉ s1.
Proof.
move => Hnotin Hsubset Hin.
apply: Hnotin.
by apply: set_in_weaken.
Qed.

(* lemmas about union *)
Lemma set_in_union_l a: a ∈ s1 -> a ∈ s1 ∪ s2.
Proof.
rewrite !setE.
by left.
Qed.

Lemma set_in_union_r a: a ∈ s2 -> a ∈ s1 ∪ s2.
Proof.
rewrite !setE.
by right.
Qed.

Lemma set_union_mono_l: s2 ⊆ s3 -> s1 ∪ s2 ⊆ s1 ∪ s3.
Proof.
rewrite !setE => Hsubset23.
split;
    move => a Hin;
    rewrite !setE.
    by left.
right.
by apply: Hsubset23.
Qed.

Lemma set_union_mono_r: s1 ⊆ s2 -> s1 ∪ s3 ⊆ s2 ∪ s3.
Proof.
rewrite !setE => Hsubset12.
split;
    move => a Hin;
    rewrite !setE.
    left.
    by apply: Hsubset12.
by right.
Qed.

Lemma set_union_mono: s1 ⊆ s2 -> s3 ⊆ s4 -> s1 ∪ s3 ⊆ s2 ∪ s4.
Proof.
rewrite !setE => Hsubset12 Hsubset34.
split;
    move => a Hin;
    rewrite !setE.
    left.
    by apply: Hsubset12.
right.
by apply: Hsubset34.
Qed.

Lemma set_union_comm : s1 ∪ s2 = s2 ∪ s1.
Proof.
rewrite !setE => a.
rewrite !setE.
split;
    move => [|];
    by [left|right].
Qed.

Lemma set_union_cancel_l: s3 ## s1 -> s3 ## s2 -> s3 ∪ s1 = s3 ∪ s2 -> s1 = s2.
Proof.
rewrite !setE.
setoid_setE_rewrite => Hdisjoint31 Hdisjoint32 Hunion a.
split.
    move => Hin1.
    have : a ∈ s3 \/ a ∈ s2.
        rewrite -Hunion.
        by right.
    move => [Hin3|].
        by have := Hdisjoint31 a Hin3 Hin1.
    by [].
move => Hin2.
have : a ∈ s3 \/ a ∈ s1.
    rewrite Hunion.
    by right.
move => [Hin3|].
    by have := Hdisjoint32 a Hin3 Hin2.
by [].
Qed.

Lemma set_union_cancel_r : s1 ## s3 -> s2 ## s3 -> s1 ∪ s3 = s2 ∪ s3 -> s1 = s2.
Proof.
rewrite !setE.
setoid_setE_rewrite => Hdisjoint31 Hdisjoint32 Hunion a.
split.
    move => Hin1.
    have : a ∈ s2 \/ a ∈ s3.
        rewrite -Hunion.
        by left.
    move => [Hin2|Hin3].
        by [].
    by have := Hdisjoint31 a Hin1 Hin3.
move => Hin2.
have : a ∈ s1 \/ a ∈ s3.
    rewrite Hunion.
    by left.
move => [|Hin3].
    by [].
by have := Hdisjoint32 a Hin2 Hin3.
Qed.

(** lemmas about ∅ *)
Lemma set_empty_subseteq: ∅ ⊆ s1.
Proof.
rewrite !setE => a.
by rewrite setE.
Qed.

Lemma set_union_positive_l: s1 ∪ s2 = ∅ -> s1 = ∅.
Proof.
rewrite set_empty_union.
by move => [].
Qed.

Lemma set_union_positive_l_alt: s1 <> ∅ -> s1 ∪ s2 <> ∅.
Proof.
move => Hneq Heq.
apply: Hneq.
by apply: set_union_positive_l.
Qed.

Lemma set_non_empty_inhabited a: a ∈ s1 -> s1 <> ∅.
Proof.
rewrite setE => Hin Hnotin.
by apply: (Hnotin a).
Qed.

(* lemmas about singleton *)
Lemma set_not_in_singleton_iff a y : a ∉ ({[ y ]}: SetType T e) <-> a <> y.
Proof.
by rewrite set_in_singleton_iff.
Qed.

Lemma set_singleton_subseteq a y : {[ a ]} ⊆ ({[ y ]}: SetType T e) <-> a = y.
Proof.
rewrite !setE.
split;
    by move ->.
Qed.

(** lemmas for disjoint *)
Lemma set_disjoint_sym : s1 ## s2 -> s2 ## s1.
Proof.
rewrite !setE.
move => Hdisjoint a Hin1 Hin2.
by apply: (Hdisjoint a).
Qed.

Lemma set_disjoint_empty_l: ∅ ## s2.
Proof.
rewrite !setE => a.
by rewrite setE.
Qed.

Lemma set_disjoint_empty_r: s1 ## ∅.
Proof.
rewrite !setE => a.
by rewrite setE.
Qed.

(*Lemmas about intersection *)
Lemma set_subseteq_intersection: s1 ⊆ s2 <-> s1 ∩ s2 = s1.
Proof.
rewrite !setE.
setoid_setE_rewrite.
split;
    last first.
    move => Hintersect a.
    rewrite -Hintersect.
    by move => [].
move => Hinin a.
split.
    by move => [].
move => Hin1.
split.
    by [].
by apply: Hinin.
Qed.

Lemma set_intersection_subseteq_l: s1 ∩ s2 ⊆ s1.
Proof.
rewrite !setE => a.
rewrite !setE.
by move => [].
Qed.

Lemma set_intersection_subseteq_r: s1 ∩ s2 ⊆ s2.
Proof.
rewrite !setE => a.
rewrite !setE.
by move => [].
Qed.

Lemma set_intersection_greatest: s3 ⊆ s1 -> s3 ⊆ s2 -> s3 ⊆ s1 ∩ s2.
Proof.
rewrite !setE => Hsubset31 Hsunset32 a.
rewrite !setE.
move => Hin3.
have ? := Hsubset31 _ Hin3.
have ? := Hsunset32 _ Hin3.
by split.
Qed.

Lemma set_intersection_mono_l: s2 ⊆ s3 -> s1 ∩ s2 ⊆ s1 ∩ s3.
Proof.
rewrite !setE => Hsubset a.
rewrite !setE.
move => [Hin1 Hin2].
split.
    by [].
by apply: Hsubset.
Qed.

Lemma set_intersection_mono_r: s1 ⊆ s3 -> s1 ∩ s2 ⊆ s3 ∩ s2.
Proof.
rewrite !setE => Hsubset a.
rewrite !setE.
move => [Hin1 Hin2].
split.
    by apply: Hsubset.
by [].
Qed.

Lemma set_intersection_mono:
  s1 ⊆ s2 -> s3 ⊆ s4 -> s1 ∩ s3 ⊆ s2 ∩ s4.
Proof.
rewrite !setE => Hsubset12 Hsubset34 a.
rewrite !setE.
move => [Hin1 Hin3].
have := Hsubset12 _ Hin1.
have := Hsubset34 _ Hin3.
by split.
Qed.

Lemma set_intersection_comm : s1 ∩ s2 = s2 ∩ s1.
Proof.
rewrite !setE => a.
rewrite !setE.
split;
    move => [];
    by split.
Qed.

Lemma set_intersection_singletons x : {[x]} ∩ {[x]} = ({[x]}: SetType T e).
Proof.
rewrite !setE => a.
rewrite !setE.
by split => [->|->].
Qed.

Lemma set_union_intersection_l: s1 ∪ (s2 ∩ s3) = (s1 ∪ s2) ∩ (s1 ∪ s3).
Proof.
rewrite !setE => a.
rewrite !setE.
split.
    move => [?|[? ?]];
    split;
        by [left|right].
move => [] [?|?] [?|?];
    by [left|right].
Qed.

Lemma set_union_intersection_r: (s1 ∩ s2) ∪ s3 = (s1 ∪ s3) ∩ (s2 ∪ s3).
Proof.
rewrite !setE => a.
rewrite !setE.
split.
    move => [[? ?] | ?];
    split;
        by [left|right].
move => [] [?|?] [?|?];
    by [left|right].
Qed.

Lemma set_intersection_union_l: s1 ∩ (s2 ∪ s3) = (s1 ∩ s2) ∪ (s1 ∩ s3).
Proof.
rewrite !setE => a.
rewrite !setE.
split.
    move => [? [? | ?]];
    by [left|right].
move => [[? ?]|[? ?]];
    split;
    by [left|right|].
Qed.

Lemma set_intersection_union_r: (s1 ∪ s2) ∩ s3 = (s1 ∩ s3) ∪ (s2 ∩ s3).
Proof.
rewrite !setE => a.
rewrite !setE.
split.
    move => [] [|];
    by [left|right].
move =>[[]|[]];
    split;
    by [left|right|].
Qed.

Lemma set_disjoint_intersection: s1 ## s2 <-> s1 ∩ s2 = ∅.
Proof.
rewrite !setE.
setoid_setE_rewrite.
split.
    move => Hdisjoint a [Hin1 Hin2].
    by apply: (Hdisjoint a).
move => Hnotin a Hin1 Hin2.
by apply: (Hnotin a).
Qed.

Lemma set_not_in_intersection a: a ∉ s1 ∩ s2 <-> a ∉ s1 \/ a ∉ s2.
Proof.
rewrite setE.
split;
case (@set_inP _ _ a s1) => Hin1.
-   move => Hnotin.
    right.
    move => Hin2.
    by apply: Hnotin.
-   move => Hnotin.
    by left.
-   move => [Hnotin1|Hnotin2].
        by [].
    by move => [].
-   by move => [_|Hnotin2] [].
Qed.

(** Difference *)

Lemma set_subseteq_empty_difference: s1 ⊆ s2 -> s1 \ s2 = ∅.
Proof.
rewrite !setE => Hsubset a.
rewrite !setE.
move => [Hin1 Hnotin2].
by have := Hsubset a Hin1.
Qed.

Lemma set_difference_union_distr_l: (s1 ∪ s2) \ s3 = s1 \ s3 ∪ s2 \ s3.
Proof.
rewrite !setE => a.
rewrite !setE.
split.
    move => [[?|?]?];
    by [left|right].
move => [[? ?]|[? ?]];
    split;
    by [left|right|].
Qed.

Lemma set_difference_union_distr_r: s3 \ (s1 ∪ s2) = (s3 \ s1) ∩ (s3 \ s2).
Proof.
rewrite !setE => a.
rewrite !setE.
split.
    by move => [?[??]].
by move => [[??][??]].
Qed.

Lemma set_difference_intersection_distr_l: (s1 ∩ s2) \ s3 = s1 \ s3 ∩ s2 \ s3.
Proof.
rewrite !setE => a.
rewrite !setE.
split.
    by move => [[??]?].
by move => [[[??]?]?].
Qed.

Lemma set_difference_disjoint: s1 ## s2 -> s1 \ s2 = s1.
Proof.
rewrite !setE => Hdisjoint a.
rewrite !setE.
split.
    by move => [].
move => Hin1.
split.
    by [].
move => Hin2.
by apply: (Hdisjoint a).
Qed.

Lemma set_subset_difference_in a: a ∈ s1 -> s1 \ {[ a ]} ⊂ s1.
Proof.
rewrite !setE => Hin1.
setoid_setE_rewrite.
split.
    by move => a' [].
move => HFalse.
have := HFalse a Hin1.
by move => [].
Qed.

Lemma set_difference_difference: (s1 \ s2) \ s3 = s1 \ (s2 ∪ s3).
Proof.
rewrite !setE => a.
rewrite !setE.
split.
    by move => [] [].
by move => [?] [].
Qed.

Lemma set_difference_mono:
  s1 ⊆ s2 -> s3 ⊆ s4 -> s1 \ s4 ⊆ s2 \ s3.
Proof.
rewrite !setE => Hsubset12 Hsubset34 a.
rewrite !setE.
move => [Hin1 Hnotin4].
split.
    by have ? := Hsubset12 a Hin1.
move => Hin3.
apply: Hnotin4.
by have := Hsubset34 a Hin3.
Qed.

Lemma set_difference_mono_l: s2 ⊆ s3 -> s1 \ s3 ⊆ s1 \ s2.
Proof.
rewrite !setE => Hsubset23 a.
rewrite !setE.
move => [Hin1 Hnotin3].
split.
    by [].
move => Hin2.
by have := Hsubset23 a Hin2.
Qed.

Lemma set_difference_mono_r: s1 ⊆ s3 -> s1 \ s2 ⊆ s3 \ s2.
Proof.
rewrite !setE => Hsubset13 a.
rewrite !setE.
move => [Hin1 Hnotin2].
have := Hsubset13 _ Hin1.
by split.
Qed.

Lemma set_subseteq_difference_r:
  s1 ## s2 -> s1 ⊆ s3 -> s1 ⊆ s3 \ s2.
Proof.
rewrite !setE => Hdisjoint12 Hsubset13 a Hin1.
rewrite !setE.
have ? := Hsubset13 a Hin1.
have ? := Hdisjoint12 a Hin1.
by split.
Qed.

Lemma set_subseteq_difference_l: s1 ⊆ s2 -> s1 \ s3 ⊆ s2.
Proof.
rewrite !setE => Hsubset1 a.
rewrite !setE.
move => [Hin1 Hnotin3].
by apply: Hsubset1.
Qed.

Lemma set_disjoint_difference_l1: s2 ⊆ s3 -> s1 \ s3 ## s2.
Proof.
rewrite !setE => Hsubset23 a.
rewrite !setE.
move => [Hin1 Hnotin3 Hin2].
apply: Hnotin3.
by apply: Hsubset23.
Qed.

Lemma set_disjoint_difference_l2: s1 ## s2 -> s1 \ s3 ## s2.
Proof.
rewrite !setE => Hdisjoint12 a.
rewrite !setE.
move => [Hin1 Hnotin3] Hin2.
by have := Hdisjoint12 a Hin1 Hin2.
Qed.

Lemma set_disjoint_difference_r1: s1 ⊆ s2 -> s1 ## s3 \ s2.
Proof.
rewrite !setE => Hsubset12 a Hin1.
rewrite !setE.
move => [Hin3 Hnotin2].
apply: Hnotin2.
by apply: Hsubset12.
Qed.

Lemma set_disjoint_difference_r2: s1 ## s3 -> s1 ## s3 \ s2.
Proof.
rewrite !setE => Hdisjoint13 a Hin1.
rewrite !setE.
move => [Hin3 Hnotin2].
by have := Hdisjoint13 a Hin1 Hin3.
Qed.

Lemma set_notin_difference a: a ∉ s1 \ s2 <-> a ∉ s1 \/ a ∈ s2.
Proof.
rewrite setE.
case (@set_inP _ _ a s2) => [Hin2 | Hnotin2];
    split.
-   move => Hnotin12.
    by right.
-   by move => [Hnotin1 | Hin2'] [].
-   move => notin12.
    left.
    move => Hin1.
    by apply: notin12.
-   by move => [Hnotin1| Hin2] [].
Qed.

Lemma set_union_difference (s: SetType T e) : s ⊆ s2 -> s2 = s ∪ s2 \ s.
Proof.
rewrite !setE.
move => Hsubset a.
rewrite !setE.
split;
    last first.
    move => [|[]] //.
    apply: Hsubset.
case (@set_inP _ _ a s).
    by left.
by right.
Qed.

Lemma set_union_difference_singleton a: a ∈ s2 -> s2 = {[a]} ∪ s2 \ {[a]}.
Proof.
move => Hin1.
apply: set_union_difference.
by rewrite setE.
Qed.

Lemma set_difference_union: s1 \ s2 ∪ s2 = s1 ∪ s2.
Proof.
rewrite setE => a.
rewrite !setE.
split.
    move => [[]|].
        by left.
    by right.
case (@set_inP _ _ a s2) => [Hin2| Hnotin2] [Hin1| Hin2'];
    by [left|right].
Qed.

Lemma set_subseteq_disjoint_union:
    s1 ⊆ s2 <-> exists s, s2 = s1 ∪ s /\ s1 ## s.
Proof.
rewrite setE.
setoid_setE_rewrite.
split;
    last first.
    move => [s'] [Hunion Hdisjoint] a Hin1.
    rewrite Hunion.
    by left.
move => Hsubset.
exists (s2 \ s1).
split => a;
    rewrite setE;
    last first.
    by move => ? [].
split;
    last first.
    move => [|[]].
        by apply: Hsubset.
    by [].
case (@set_inP _ _ a s1) => [Hin1| Hnotin1].
    by left.
by right.
Qed.

Lemma set_non_empty_difference: s1 ⊂ s2 -> s2 \ s1 <> ∅.
Proof.
move => [Hsubset12 Hnotsubset21] Hdiff.
apply: Hnotsubset21.
move :Hsubset12 Hdiff.
rewrite !setE.
setoid_setE_rewrite.
move => Hsubset12 Hdiff a Hin2.
case (@set_inP _ _ a s1) => [Hin1|Hnotin1].
    by [].
have HFalse:= (Hdiff a).
by contradict HFalse.
Qed.

Lemma set_empty_difference_subseteq: s1 \ s2 = ∅ -> s1 ⊆ s2.
Proof.
rewrite !setE.
setoid_setE_rewrite.
move => Hdifference a Hin1.
case (@set_inP _ _ a s2) => [Hin2|Hnotin2].
    by [].
have HFalse := Hdifference a.
by contradict HFalse.
Qed.

Lemma set_singleton_union_difference a:
  {[a]} ∪ (s1 \ s2) = ({[a]} ∪ s1) \ (s2 \ {[a]}).
Proof.
rewrite setE => b.
rewrite !setE.
split.
    move => [->|[Hinb1 Hnotinb2]];
    split.
    +   by left.
    +   by move =>[].
    +   by right.
    +   by move => [].
move => [[->|Hinb1] Hnotin].
    by left.
case (@eqP _ b a) => [-> |Hneq].
    by left.
right.
case (@set_inP _ _ b s2) => [Hinb2 | Hnotinb2];
    last by [].
by contradict Hnotin.
Qed.

End lemmas.

(* lemmas about set_forall and set_exists *)
Section quantifiers.
Context (T: eqType).
Context (e: finSet T).
Context (s s1 s2: SetType T e).
Context (P Q: T -> Prop).

Lemma set_forall_empty : set_forall P (∅ :SetType T e).
Proof.
rewrite /set_forall => a.
by rewrite !setE.
Qed.

Lemma set_forall_singleton a: set_forall P ({[ a ]} : SetType T e) <-> P a.
Proof.
rewrite /set_forall.
setoid_setE_rewrite.
split.
    move => Hpa.
    by apply: Hpa.
by move => HP a' ->.
Qed.

Lemma set_forall_union:
  set_forall P s1 -> set_forall P s2 -> set_forall P (s1 ∪ s2).
Proof.
rewrite /set_forall => HP1 HP2 a.
rewrite !setE.
move => [Hin1|Hin2].
    by apply: HP1.
by apply: HP2.
Qed.

Lemma set_forall_union_inv_1: set_forall P (s1 ∪ s2) -> set_forall P s1.
Proof.
rewrite /set_forall.
setoid_setE_rewrite.
move => Hforallunion a Hin.
apply: Hforallunion.
by left.
Qed.

Lemma set_forall_union_inv_2: set_forall P (s1 ∪ s2) -> set_forall P s2.
Proof.
rewrite /set_forall.
setoid_setE_rewrite.
move => Hforallunion a Hin.
apply: Hforallunion.
by right.
Qed.

Lemma set_exists_empty : ~set_exists P (∅ : SetType T e).
Proof.
rewrite /set_exists.
setoid_setE_rewrite.
by move => [?[]].
Qed.

Lemma set_exists_singleton x : set_exists P ({[ x ]} : SetType T e) <-> P x.
Proof.
rewrite /set_exists.
setoid_setE_rewrite.
split.
    by move => [? [->]].
move => HP.
by exists x.
Qed.

Lemma set_exists_union_1: set_exists P s1 -> set_exists P (s1 ∪ s2).
Proof.
rewrite /set_exists.
move => [a [Hin Hp]].
exists a.
rewrite !setE.
split.
    by left.
by [].
Qed.

Lemma set_exists_union_2: set_exists P s2 -> set_exists P (s1 ∪ s2).
Proof.
rewrite /set_exists.
move => [a [Hin Hp]].
exists a.
rewrite !setE.
split.
    by right.
by [].
Qed.

Lemma set_exists_union_inv:
  set_exists P (s1 ∪ s2) -> set_exists P s1 \/ set_exists P s2.
Proof.
rewrite /set_exists.
setoid_setE_rewrite.
move => [a [[Hin1|Hin2] Hp]].
    left.
    by exists a.
right.
by exists a.
Qed.

Lemma set_forall_impl:
    set_forall P s -> (forall a, P a -> Q a) -> set_forall Q s.
Proof.
rewrite /set_forall => HP Himp a Hin.
apply: Himp.
by apply: HP.
Qed.

Lemma set_exists_imp:
    set_exists P s -> (forall a, P a -> Q a) -> set_exists Q s.
Proof.
rewrite /set_exists.
move => [a [Hin Hp]] Himp.
exists a.
split.
    by [].
by apply: Himp.
Qed.
End quantifiers.

(* properties for list_to_set *)
Lemma set_in_list_to_set1 T (e: finSet T) l a :
    a ∈ (list_to_set l : SetType T e) -> PIn a l.
Proof.
elim : l => [| a' l IHl] /=.
    by rewrite setE.
rewrite !setE PIn_cons_or.
move => [->| Hin].
    by left.
right.
by apply: IHl.
Qed.

Lemma set_in_list_to_set2 T (e: finSet T) l a :
    PIn a l -> a ∈ (list_to_set l : SetType T e).
Proof.
elim => {a l} [a l|a b l Hin IHl] /=;
    rewrite !setE.
    by left.
by right.
Qed.

Lemma set_in_list_to_set T (e: finSet T) l a :
    a ∈ (list_to_set l : SetType T e) <-> PIn a l.
Proof.
split.
    by apply: set_in_list_to_set1.
by apply: set_in_list_to_set2.
Qed.

Lemma set_not_in_list_to_set T (e: finSet T) l a :
    a ∉ (list_to_set l : SetType T e) <-> ~ PIn a l.
Proof.
by rewrite set_in_list_to_set.
Qed.

Lemma list_to_set_empty T (e: finSet T): list_to_set [::] = (∅: SetType T e) .
Proof.
by [].
Qed.

Lemma list_to_set_cons T (e: finSet T) x l:
    list_to_set (x :: l) = ({[x]} ∪ list_to_set l : SetType T e).
Proof.
by [].
Qed.

Lemma list_to_set_app T (e: finSet T) l1 l2:
    (list_to_set (l1 ++ l2) : SetType T e) = list_to_set l1 ∪ list_to_set l2.
Proof.
rewrite setE => a.
rewrite setE.
by rewrite set_in_list_to_set PIn_app !set_in_list_to_set.
Qed.

Lemma list_to_set_singleton T (e: finSet T) x :
    list_to_set [::x] = ({[ x ]} : SetType T e).
Proof.
rewrite setE => a /=.
rewrite !setE.
split.
    by move => [].
by left.
Qed.
Lemma list_to_set_app_sigleton T (e: finSet T) l x :
    (list_to_set (l ++ [::x]): SetType T e) = list_to_set l ∪ {[ x ]}.
Proof.
rewrite setE => a /=.
rewrite list_to_set_app !setE.
split.
    move => [|[->|]];
        by [left|right|].
move =>[|->].
    by left.
right.
by left.
Qed.

(* properties for set_to_list *)
Lemma set_to_list_empty T (e: finSet T): set_to_list (∅: SetType T e) = [::].
Proof.
rewrite PIn_empty => x.
by rewrite -set_in_to_list_iff setE.
Qed.

Lemma set_to_list_empty_iff T (e: finSet T) (s: SetType T e):
    set_to_list s = [::] <-> s = ∅.
Proof.
split;
    last first.
    move ->.
    by apply: set_to_list_empty.
move => Heq.
rewrite setE => a.
rewrite set_in_to_list_iff Heq.
by apply: PIn_empty_false.
Qed.

Lemma set_to_list_union_singleton T (e: finSet T) (s: SetType T e) a :
    a ∉ s -> set_to_list ({[ a ]} ∪ s) ≡ₚ a :: set_to_list s.
Proof.
move => Hnotin.
apply: Puniq_Permutation.
-   by apply: set_unique_to_list.
-   apply: puniq_cons.
        by rewrite -set_in_to_list_iff.
    by apply: set_unique_to_list.
move => a'.
rewrite PIn_cons_or -!set_in_to_list_iff.
by rewrite set_in_union_iff set_in_singleton_iff.
Qed.

Lemma set_to_list_singleton T (e: finSet T) a:
    set_to_list ({[a]} : SetType T e) = [:: a].
Proof.
rewrite -(set_union_empty_r {[a]}) perm_singleton_r.
suff: set_to_list ({[a]} ∪ (∅ : SetType T e)) ≡ₚ
      a::set_to_list (∅ : SetType T e).
    by rewrite !setE set_to_list_empty.
apply: set_to_list_union_singleton.
by rewrite setE.
Qed.

Lemma set_to_list_disjoint_union T (e: finSet T) (s1 s2 : SetType T e):
    s1 ## s2 -> set_to_list (s1 ∪ s2 ) ≡ₚ set_to_list s1 ++ set_to_list s2.
Proof.
rewrite setE.
move => Hspec.
apply: Puniq_Permutation.
-   by apply: set_unique_to_list.
-   rewrite puniq_app.
    split.
        by apply: set_unique_to_list.
    split.
        by apply: set_unique_to_list.
    move => x.
    rewrite -!set_in_to_list_iff.
    by apply: Hspec.
move => x.
by rewrite PIn_app -!set_in_to_list_iff setE.
Qed.

Lemma set_to_list_to_set T (e: finSet T) (s: SetType T e):
    list_to_set (set_to_list s) = s.
Proof.
rewrite setE => a.
by rewrite set_in_list_to_set set_in_to_list_iff.
Qed.

Lemma list_to_set_to_list T (e: finSet T) l:
    PUniq l -> set_to_list ((list_to_set l) : SetType T e) ≡ₚ l.
Proof.
elim => {l} [|a l Hnotin Huniq IHl] /=.
    rewrite set_to_list_empty.
    apply: perm_refl.
have : set_to_list (({[a]} ∪  list_to_set l) : SetType T e) ≡ₚ
        set_to_list ({[a]} : SetType T e) ++
        (set_to_list ((list_to_set l) : SetType T e)).
    apply: set_to_list_disjoint_union.
    by rewrite setE set_not_in_list_to_set.
move => HP.
apply: permutation_trans.
    apply: set_to_list_disjoint_union.
    by rewrite setE set_not_in_list_to_set.
rewrite set_to_list_singleton /=.
by apply: permutation_skip.
Qed.

(* lemmas about set_size *)
Lemma set_size_empty T (e: finSet T):
    set_size (∅ : SetType T e) = 0.
Proof.
by rewrite /set_size set_to_list_empty.
Qed.

Lemma set_size_empty_iff T (e: finSet T) (s: SetType T e):
    set_size s = 0 <-> s = ∅.
Proof.
split;
    last first.
    move ->.
    by rewrite set_size_empty.
rewrite setE /set_size length_zero_iff_nil => Hempty a.
by rewrite set_in_to_list_iff Hempty PIn_empty_false_iff.
Qed.

Lemma set_choose_or_empty T (e: finSet T) (s: SetType T e) :
    (exists x, x ∈ s) \/ s = ∅.
Proof.
move Hl2s : (set_to_list s) => [| a l].
    right.
    by rewrite -set_to_list_empty_iff.
left.
exists a.
rewrite set_in_to_list_iff Hl2s.
apply: pin_here.
Qed.

Lemma set_choose T (e: finSet T) (s: SetType T e):
    s <> ∅ -> exists x, x ∈ s.
Proof.
move => Hneq.
by move : (@set_choose_or_empty _ _ s) => [].
Qed.

Lemma set_size_pos_elem_of T (e: finSet T) (s: SetType T e):
    0 < set_size s -> exists x, x ∈ s.
Proof.
move : (@set_choose_or_empty _ _ s) => [|->].
    by [].
rewrite set_size_empty.
lia.
Qed.


Lemma set_size_singleton T (e: finSet T) (s: SetType T e) a:
    set_size ({[a]} : SetType T e) = 1.
Proof.
by rewrite /set_size set_to_list_singleton.
Qed.

Lemma set_size_singleton_inv T (e: finSet T) (s: SetType T e) a b:
    set_size s = 1 -> a ∈ s -> b ∈ s -> a = b.
Proof.
rewrite /set_size !set_in_to_list_iff.
move : (set_to_list s) => l.
case : l => [| a' l] /=.
    by [].
case : l => [| a'' l] /=;
    last by [].
rewrite -!PIn_singlton.
by move => _ -> ->.
Qed.

Lemma size_1_elem_of T (e: finSet T) (s: SetType T e):
    set_size s = 1 -> exists x, s = {[ x ]}.
Proof.
move => Hsize.
move : (@set_size_pos_elem_of _ _ s) => [|a Hin].
    by lia.
exists a.
symmetry.
rewrite setE => a'.
rewrite !setE.
split.
    by move ->.
move => Hin'.
by apply: (@set_size_singleton_inv _ _ s).
Qed.

Lemma set_size_union T (e: finSet T) (s1 s2: SetType T e):
    s1 ## s2 -> set_size (s1 ∪ s2) = set_size s1 + set_size s2.
Proof.
rewrite /set_size -list_app_length setE => Hspec.
apply: perm_length.
apply:Puniq_Permutation.
-   by apply: set_unique_to_list.
-   rewrite puniq_app.
    split.
        by apply: set_unique_to_list.
    split.
        by apply: set_unique_to_list.
    move => a.
    rewrite -!set_in_to_list_iff.
    by apply: Hspec.
move => a.
by rewrite PIn_app -!set_in_to_list_iff setE.
Qed.

Lemma set_size_union_alt T (e: finSet T) (s1 s2: SetType T e):
    set_size (s1 ∪ s2) = set_size s1 + set_size (s2 \ s1).
Proof.
rewrite -set_size_union;
    last first.
    rewrite setE => a.
    by rewrite !setE => ? [].
have : s1 ∪ s2 \ s1 = s1 ∪ (s1 ∪ s2) \ s1.
    rewrite setE => a.
    rewrite !setE.
    split.
        move => [Hin1|[Hin2 Hnotin1]].
            by left.
        right.
        split => //.
        by right.
    move => [Hin1|[]].
        by left.
    move => [Hin1|Hin2].
        by [].
    by right.
move ->.
rewrite <-set_union_difference.
    by [].
by apply: set_union_subseteq_l.
Qed.

Lemma set_size_difference T (e: finSet T) (s1 s2: SetType T e):
    s2 ⊆ s1 -> set_size (s1 \ s2) = set_size s1 - set_size s2.
Proof.
move => Hsubset21.
have : set_size s1 = set_size (s2 ∪ s1 \ s2).
    by rewrite -(@ set_union_difference _ _ s1 s2).
move ->.
rewrite set_size_union.
    by lia.
move: Hsubset21.
rewrite !setE.
move => Hsubset a.
rewrite setE.
by move => Hin2 [].
Qed.

Lemma set_size_difference_alt T (e: finSet T) (s1 s2: SetType T e):
    set_size (s1 \ s2) = set_size s1 - set_size (s1 ∩ s2).
Proof.
rewrite -set_size_difference;
    last by apply: set_intersection_subseteq_l.
f_equal.
rewrite setE => a.
rewrite !setE.
split.
    move => [Hnotin1 Hnot2].
    split =>//.
    by move => [].
move => [Hin Hnotinin].
split => //.
move => Hin2.
by apply: Hnotinin.
Qed.

Lemma set_subseteq_size T (e: finSet T) (s1 s2: SetType T e):
    s1 ⊆ s2 -> set_size s1 <= set_size s2.
Proof.
move => Hsubset.
rewrite (@set_union_difference _ _ s2 s1).
rewrite set_size_union_alt.
    by lia.
by [].
Qed.

Lemma set_subset_size T (e: finSet T) (s1 s2: SetType T e):
    s1 ⊂ s2 -> set_size s1 < set_size s2.
Proof.
move => [Hsubset12 Hnotsubset21].
rewrite (@set_union_difference _ _ s2 s1);
    last by [].
rewrite set_size_union_alt set_difference_twice.
suff : set_size (s2 \ s1) <> 0.
    by lia.
rewrite set_size_empty_iff.
by apply: set_non_empty_difference.
Qed.

Lemma set_size_list_to_set T (e: finSet T) l :
  PUniq l -> set_size ((list_to_set l): SetType T e) = length l.
Proof.
rewrite /set_size => Huniq.
apply: perm_length.
by apply:list_to_set_to_list.
Qed.

(* set induction *)
Lemma set_subseteq_well_founded T (e: finSet T):
    well_founded (@set_subset T e).
Proof.
rewrite /well_founded.
suff : forall n,
    Acc lt n ->
    forall (s: SetType T e), n = set_size s ->
    Acc (@set_subset T e) s.
    move => Htrans a.
    apply: (Htrans);
        last by [].
    by apply: Wf_nat.lt_wf.
move => n.
elim => [y _ IH] s Heq.
move : Heq IH => -> IH.
clear y.
constructor => y.
move /set_subset_size => Hsize.
apply: IH;
    last by [].
by lia.
Qed.

Lemma set_ind T (e: finSet T) (P: SetType T e -> Prop):
    P ∅ ->
    (forall a s, a ∉ s -> P s -> P ({[a]} ∪ s)) ->
    forall s, P s.
Proof.
move => Hempty Hunion.
apply: well_founded_induction.
    by apply: set_subseteq_well_founded.
move => s HsubsetP.
move : (set_choose_or_empty s) => [[a Hins]|->];
    last by [].
rewrite (@set_union_difference _ _ s {[a]});
    last by rewrite setE.
apply: Hunion.
    rewrite !setE.
    by move => [].
apply: HsubsetP.
by apply: set_subset_difference_in.
Qed.

(* lemmas for set_fold *)
Lemma set_fold_empty T T' (e: finSet T) (f: T-> T' -> T') b:
    set_fold f b (∅ : SetType T e) = b.
Proof.
by rewrite /set_fold set_to_list_empty.
Qed.

Lemma set_fold_singleton T T' (e: finSet T) (f : T -> T' -> T') a b:
  set_fold f b ({[a]} : SetType T e) = f a b.
Proof.
by rewrite /set_fold set_to_list_singleton.
Qed.
Lemma set_fold_disj_union T (e: finSet T) (f : T -> T -> T)
                          (s1 s2: SetType T e) b:
  commutative f ->
  associative f ->
  s1 ## s2 ->
  set_fold f b (s1 ∪ s2) = set_fold f (set_fold f b s1) s2.
Proof.
move => Hcom Hassoc Hdisjoint.
rewrite /set_fold -foldr_cat.
apply: foldr_perm => //=.
apply: permutation_trans.
    by apply: set_to_list_disjoint_union.
by apply: perm_catC.
Qed.
