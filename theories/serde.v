(*****************************************************************************)
(* Copyright 2022 TheWalker77                                                *)
(*                                                                           *)
(* Licensed under the Apache License, Version 2.0 (the "License");           *)
(* you may not use this file except in compliance with the License.          *)
(* You may obtain a copy of the License at                                   *)
(*                                                                           *)
(*     http://www.apache.org/licenses/LICENSE-2.0                            *)
(*                                                                           *)
(* Unless required by applicable law or agreed to in writing, software       *)
(* distributed under the License is distributed on an "AS IS" BASIS,         *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  *)
(* See the License for the specific language governing permissions and       *)
(* limitations under the License.                                            *)
(*****************************************************************************)
From mathcomp Require Import all_ssreflect zify.
From ssrmap Require Import pos.
Import Pos.

Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.


(*****************************************************************************)
(* This file implements basic serialization/deserialization cannonical       *)
(* structure. The to be serializable data type must have two functions:      *)
(* [serialize] and [deserialize]. [serialize] convert type "T" to [positive] *)
(* while [deserialize] converts an arbitrary [positive] back to "T".         *)
(* It is important to note that [deserialize] must be right and left inverse *)
(* for [serialize].                                                          *)
(*****************************************************************************)


Module SerDe.
Record operations (T: Type) := Operations {
    Serialize: T -> positive;
    Deserialize: positive -> T;
}.

Record lemmas (T: Type) (o: operations T) := Lemmas {
    Deser: forall (a: T), Deserialize o (Serialize o a) = a;
    Serde: forall (p: positive), Serialize o (Deserialize o p) = p;
}.

Structure type :=  Pack{
    CST: Type;
    ops: operations CST;
    lms: lemmas ops;
}.

End SerDe.

Coercion SerDe.CST : SerDe.type >-> Sortclass.

Import SerDe.
Definition serDe := SerDe.type.

(* basic operations *)
Definition serialize (cs: serDe) : CST cs -> positive := Serialize (ops cs).
Arguments serialize {cs} t: simpl never.

Definition deserialize (cs: serDe) : positive ->CST cs := Deserialize (ops cs).
Arguments deserialize {cs} p: simpl never.

(* basic lemmas *)

Lemma deserialize_serialize {cs: serDe}:
    forall (a: CST cs), deserialize (serialize a) = a.
Proof Deser (lms cs).

Lemma serialize_deserialize {cs: serDe}:
    forall (p: positive), serialize ((deserialize p): CST cs) = p.
Proof Serde (lms cs).

(* derived lemmas *)

Lemma serialize_inv (T: serDe):
    forall (a b: T), a = b <-> serialize a = serialize b.
Proof.
move => a b.
split.
    by move ->.
move => Hser.
have : (deserialize (serialize a) : T) = deserialize (serialize b).
    by rewrite Hser.
by rewrite !deserialize_serialize.
Qed.

Lemma serialize_neq_inv (T: serDe):
forall (a b: T), a <> b <-> serialize a <> serialize b.
Proof.
move => a b.
by rewrite -serialize_inv.
Qed.

Lemma deserialize_inv (T: serDe):
forall (a b: positive), a = b <-> (deserialize a: T) = deserialize b.
Proof.
move => a b.
split.
    by move ->.
move => Hdeser.
have : serialize ((deserialize a): T) = serialize ((deserialize b): T).
    by rewrite Hdeser.
by rewrite !serialize_deserialize.
Qed.

Lemma deserialize_neq_inv (T: serDe):
forall (a b: positive), a <> b <-> (deserialize a: T) <> deserialize b.
Proof.
move => a b.
by rewrite -deserialize_inv.
Qed.

(* serDe for nat *)
Module SerDeNat.
(* operations *)
Fixpoint pos_to_nat_partial (p: positive) : nat :=
match p with
| xH => 1
| xI p' => 1 + 2 * pos_to_nat_partial p'
| xO p' => 2 * pos_to_nat_partial p'
end.

Definition pos_to_nat p := pos_to_nat_partial p - 1.

Fixpoint nat_to_pos (n: nat) : positive :=
match n with
| O => xH
| S n' => succ (nat_to_pos (n'))
end.

(* lemmas *)
Lemma pos_to_nat_partial_g0: forall p, 0 < pos_to_nat_partial p.
Proof.
elim => [p IHp | p IHp|] /=;
    by lia.
Qed.

Lemma pos_to_nat_partial_succ: forall p,
    pos_to_nat_partial (succ p) = S (pos_to_nat_partial p).
Proof.
elim => [p IHp | p IHp |] /=;
by lia.
Qed.

Lemma pos_to_nat_succ: forall p, pos_to_nat (succ p) = S (pos_to_nat p).
Proof.
move => p.
have ? := pos_to_nat_partial_g0 p.
rewrite /pos_to_nat pos_to_nat_partial_succ.
lia.
Qed.

Lemma nat_to_pos_to_nat: forall n, pos_to_nat (nat_to_pos n) = n.
Proof.
elim =>[| n IHn] /=.
    by [].
by rewrite pos_to_nat_succ IHn.
Qed.

Lemma pos_to_nat_xO: forall p,
    pos_to_nat (xO p) = 1 + 2 * pos_to_nat p.
Proof.
move => p.
rewrite /pos_to_nat /=.
have ? := pos_to_nat_partial_g0 p.
lia.
Qed.

Lemma pos_to_nat_xI: forall p,
    pos_to_nat (xI p) = 2 + 2 * pos_to_nat p.
Proof.
move => p.
rewrite /pos_to_nat /=.
have ? := pos_to_nat_partial_g0 p.
lia.
Qed.

Lemma nat_to_pos_double: forall n,
    nat_to_pos (2 * n) =  pred (xO (nat_to_pos n)).
Proof.
elim => [|n].
    by [].
rewrite !mul2n /= => IHn.
by rewrite pred'_succ IHn succ_pred'.
Qed.

Lemma pos_to_nat_to_pos: forall p, nat_to_pos (pos_to_nat p) = p.
Proof.
elim => [p IHp | p IHp |] /=.
-   rewrite pos_to_nat_xI /=.
    by rewrite nat_to_pos_double succ_pred' IHp.
-   rewrite pos_to_nat_xO /=.
    by rewrite nat_to_pos_double succ_pred' IHp.
-   by [].
Qed.

Definition serDe_nat_ops: SerDe.operations nat.
Proof.
constructor.
-   (* serialize *)
    by apply: nat_to_pos.
-   (* deserialize *)
    by apply: pos_to_nat.
Defined.

Definition serDe_nat_lemmas: SerDe.lemmas serDe_nat_ops.
Proof.
constructor => /=.
-   (* deser *)
    by apply: nat_to_pos_to_nat.
-   (* serde *)
    by apply: pos_to_nat_to_pos.
Qed.
End SerDeNat.

Canonical Structure nat_is_serde := SerDe.Pack SerDeNat.serDe_nat_lemmas.

(* This module combines SerDe with EqType. *)
Module SerDeEqType.

Record mixin T := Mixin {
    Eq_type: Equality.mixin_of T;
    SerDe_ops: SerDe.operations T;
    SerDe_lemmas: SerDe.lemmas SerDe_ops;
}.

Record type := Class {
    SEQ: Type;
    ops: mixin SEQ;
}.

Definition to_eq (t: type) : eqType :=
    Equality.Pack (Eq_type (ops t)).

Definition to_serde (t: type) : serDe :=
    SerDe.Pack (SerDe_lemmas (ops t)).

End SerDeEqType.

Canonical Structure SerDeEqType.to_eq.
Canonical Structure SerDeEqType.to_serde.
Notation serDeEqType := (SerDeEqType.type).
Coercion SerDeEqType.SEQ : serDeEqType >-> Sortclass.

Canonical Structure nat_is_serde_eq := SerDeEqType.Class
    (SerDeEqType.Mixin ssrnat.nat_eqMixin SerDeNat.serDe_nat_lemmas).
