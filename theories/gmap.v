(*****************************************************************************)
(* Copyright 2022 TheWalker77                                                *)
(*                                                                           *)
(* Licensed under the Apache License, Version 2.0 (the "License");           *)
(* you may not use this file except in compliance with the License.          *)
(* You may obtain a copy of the License at                                   *)
(*                                                                           *)
(*     http://www.apache.org/licenses/LICENSE-2.0                            *)
(*                                                                           *)
(* Unless required by applicable law or agreed to in writing, software       *)
(* distributed under the License is distributed on an "AS IS" BASIS,         *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  *)
(* See the License for the specific language governing permissions and       *)
(* limitations under the License.                                            *)
(*****************************************************************************)

From mathcomp Require Import all_ssreflect zify.
From ssrmap Require Import serde trie pos fin_maps seq.
Export serde fin_maps.

Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.


(******************************************************************************)
(* This file generalizes the trie implementation in "trie.v" into one with    *)
(* arbitrary key type and and arbitrary value type. The key must implement    *)
(* [eqType] and [SerDe] canonical structures. Both structures are combined in *)
(* [SerDeEqType.type].                                                        *)
(*                                                                            *)
(* The main data structure is [GTrie]                                         *)
(*                                                                            *)
(* The main operations:                                                       *)
(* - [lookup]                                                                 *)
(* - [singleton]                                                              *)
(* - [map]                                                                    *)
(* - [fmap]                                                                   *)
(* - [alter]                                                                  *)
(* - [to_list]                                                                *)
(* - [merge]                                                                  *)
(*                                                                            *)
(* The main lemmas:                                                           *)
(* [extensionality]                                                           *)
(* [lookup_empty]                                                             *)
(* [lookup_alter]                                                             *)
(* [lookup_alter_neq]                                                         *)
(* [lookup_fmap]                                                              *)
(* [to_list_no_dup]                                                           *)
(* [elem_of_to_list]                                                          *)
(* [lookup_map]                                                               *)
(* [lookup_merge]                                                             *)
(******************************************************************************)

Definition GTrie (K V: Type) := Trie.Trie V.

Module GTrie.

Section GTrie_Operations.
Context {K: serDeEqType}.
Context {V V' V'': Type}.

Definition GEmpty : GTrie K V := Trie.Tempty.

Definition lookup (k: K) (t: GTrie K V) : option V :=
    Trie.lookup (serialize k) t.
Global Arguments lookup: simpl never.

Definition singleton (k: K) (v: V) := Trie.singleton (serialize k) v.
Global Arguments singleton: simpl never.

Definition map (f: V -> V') (t: @GTrie K V) : GTrie K V' :=
    Trie.map f t.
Global Arguments map: simpl never.

Definition fmap (f: V -> option V') (t: GTrie K V) : GTrie K V' :=
    Trie.fmap f t.
Global Arguments fmap: simpl never.

Definition alter (f: option V -> option V) (k: K) (t: GTrie K V): GTrie K V :=
    Trie.alter f (serialize k) t.
Global Arguments alter: simpl never.

Definition to_list (t: GTrie K V) : list (K * V) :=
    seq.map (fun pv => (deserialize pv.1, pv.2)) (Trie.to_list t).
Global Arguments to_list: simpl never.

Definition merge (f: option V' -> option V'' -> option V)
                 (t1: GTrie K V') (t2: GTrie K V'') : GTrie K V :=
    Trie.merge f t1 t2.
Global Arguments merge: simpl never.
End GTrie_Operations.

Section GTrie_Props.
Context {K: serDeEqType}.
Context {V V' V'': Type}.
Lemma extensionality: forall (t1 t2: GTrie K V),
    (forall k, lookup k t1 = lookup k t2) -> t1 = t2.
Proof.
rewrite /lookup.
move => t1 t2 Heq.
apply: Trie.extensionality => p.
have := Heq (deserialize p).
by rewrite !serialize_deserialize.
Qed.

Lemma lookup_empty: forall k, lookup k (@GEmpty K V) = None.
Proof.
move => k.
by rewrite /lookup /GEmpty Trie.lookup_empty.
Qed.

Lemma lookup_alter : forall g (t: GTrie K V) k,
    lookup k (alter g k t) = g (lookup k t).
Proof.
rewrite /lookup /alter.
move => t k g.
by rewrite Trie.lookup_alter.
Qed.

Lemma lookup_alter_neq: forall g (t: GTrie K V) k1 k2,
    k1 != k2 -> lookup k1 (alter g k2 t) = (lookup k1 t).
Proof.
move => g t k1 k2 /eqP Hneq.
rewrite /lookup /alter Trie.lookup_alter_neq.
    by [].
apply/eqP.
apply/serialize_neq_inv => Heq.
by apply: Hneq.
Qed.

Lemma lookup_fmap: forall (h: V -> option V') (t: GTrie K V) k,
    lookup k (fmap h t) = obind h (lookup k t).
Proof.
move => h t k.
by rewrite /lookup /fmap Trie.lookup_fmap.
Qed.

Lemma to_list_no_dup: forall (t: GTrie K V), PUniq (to_list t).
Proof.
move => t.
rewrite /to_list.
apply: puniq_map_inv_fun;
    last by apply: Trie.to_list_no_dup.
move => [ka va] [kb vb] []/=.
rewrite -deserialize_inv.
by move => -> ->.
Qed.

Lemma elem_of_to_list: forall (t: GTrie K V) k v,
    PIn (k, v) (to_list t) <->
    lookup k t = Some v.
Proof.
move => t k v.
rewrite /to_list /lookup.
split.
    move /pin_map => [[p v']] [] [-> ->] Hin.
    by rewrite -Trie.elem_of_to_list serialize_deserialize.
rewrite -Trie.elem_of_to_list.
move => Hin.
apply/pin_map.
exists (serialize k, v) => /=.
rewrite deserialize_serialize.
by split.
Qed.

Lemma lookup_map: forall (f: V -> V') (t: GTrie K V) k,
        lookup k (map f t) = omap f (lookup k t).
Proof.
move => f t k.
by rewrite /lookup Trie.lookup_map.
Qed.

Lemma lookup_merge: forall (f: option V' -> option V'' -> option V)
                           (t1: GTrie K V') (t2: GTrie K V'') k,
    f None None = None ->
    lookup k (merge f t1 t2) = f (lookup k t1) (lookup k t2).
Proof.
move => f t1 t2 k Hnone.
by rewrite /lookup /merge Trie.lookup_merge.
Qed.

End GTrie_Props.

(* TODO: figure why coersion "SerDeEqType.to_eq K" fails *)
Definition gtrie_finmap_ops (K :serDeEqType):
    FinMap.operations (SerDeEqType.to_eq K) GTrie.
Proof.
constructor;
    move => V.
-   (* empty *)
    by apply: GEmpty.
-   (* lookup *)
    by apply: lookup.
-   (* alter*)
    by apply: alter.
-   (* map *)
    move => V2.
    by apply: map.
-   (* fmap *)
    move => V2.
    by apply: fmap.
-   (* merge *)
    move => V1 V2.
    by apply: merge.
-   (*to_list*)
    by apply: to_list.
Defined.

Definition trie_finmap_lemmas (K :serDeEqType):
    FinMap.lemmas (gtrie_finmap_ops K).
Proof.
constructor;
    move => V /=.
-   by apply: extensionality.
-   by apply: lookup_empty.
-   move => f m k.
    by rewrite lookup_alter.
-   move => f m k1 k2 Hneq.
    by rewrite lookup_alter_neq.
-   move => V2 f m k.
    by rewrite lookup_fmap.
-   by apply: to_list_no_dup.
-   by apply: elem_of_to_list.
-   move => V' f m k.
    by rewrite lookup_map.
-   move => V1 V2 f m1 m2 k Hnone.
    by rewrite lookup_merge.
Qed.

End GTrie.

Canonical Structure trie_is_fin_map (K: serDeEqType) :=
    FinMap.Pack (GTrie.trie_finmap_lemmas K).